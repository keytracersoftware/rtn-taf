/*
 * Created by Yehor Karliuchenko on 11/28/18 1:49 PM
 */

package utils;/*
 * Created by Yehor Karliuchenko on 11/28/18 1:44 PM
 */

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class Log {

    private Log() {}

    static {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm");
        System.setProperty("current.date.time", dateFormat.format(new Date()));
        PropertyConfigurator.configure("log4j.properties");
    }

    private static Logger Log = Logger.getLogger(Log.class);

    public static <T extends Object> void info(T message) {
        Log.info(String.valueOf(message));
    }

    public static <T extends Object> void warn(T message) {
        Log.warn(String.valueOf(message));
    }

    public static <T extends Object> void error(T message) {
        Log.error(String.valueOf(message));
    }
}
