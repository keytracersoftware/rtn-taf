/*
 * Created by Yehor Karliuchenko on 11/22/18 6:34 PM
 */

package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public final class Util {

    private Util() {}

    public static String getCurrentLocalDateTimeStamp() {
        return LocalDateTime.now()
                .format(DateTimeFormatter.ofPattern("dd.MM.yy_HH.mm.SS"));
    }

    public static int getRandomNumber(int min, int max){
        return new Random().nextInt((max - min) + 1) +min;
    }

    /**
     * @param pattern {@link Consts#ALPHANUMERIC_STRING} / {@link Consts#ALPHA_STRING_UPPERCASE} / {@link Consts#ALPHA_STRING_LOWERCASE} / {@link Consts#NUMERIC_STRING}
     */
    public static String getRandomString(String pattern, int length){
        StringBuilder builder = new StringBuilder();
        while (length-- !=0){
            int character = (int)(Math.random()*pattern.length());
            builder.append(pattern.charAt(character));
        }
        return builder.toString();
    }
}
