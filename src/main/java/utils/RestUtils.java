/*
 * Created by Yehor Karliuchenko on 11/22/18 6:46 PM
 */

package utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;

import java.util.Map;

import static io.restassured.config.RestAssuredConfig.newConfig;
import static io.restassured.config.SessionConfig.sessionConfig;

public final class RestUtils {
    private RestUtils() {}

    public static void setBaseURI(String baseURI) {
        if (baseURI != null && !baseURI.isEmpty()) {
            RestAssured.baseURI = baseURI;
        } else {
            System.err.println("Empty baseURL");
        }
    }

    public static void resetBaseURI() {
        RestAssured.baseURI = null;
    }

    public static Map parseIntoMap(Object objToParse) {
        Gson bGson = new GsonBuilder().create();
        return new Gson().fromJson(bGson.toJson(objToParse), Map.class);
    }

    public static String getSessionId() {
        return RestAssured.sessionId;
    }

    public static void setSessionName(String sessionCookieName) {
        RestAssured.config = newConfig().sessionConfig(sessionConfig().sessionIdName(sessionCookieName));
    }

    public static void enableCharsetAppending(boolean isEnabled) {
        RestAssured.config = RestAssured.config().encoderConfig(new EncoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(isEnabled));
    }

}
