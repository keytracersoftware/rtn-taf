/*
 * Created by Yehor Karliuchenko on 11/22/18 6:22 PM
 */

package utils;

public final class Consts {

    private Consts() {}

    public static final String USER_DIR = System.getProperty("user.dir");
    public static final String SCREENSHOT_OUT_DIR = USER_DIR + "/src/main/resources/output/screenshots/";

    public static final boolean PASSES = true;
    public static final boolean FAILS = false;

    public static final boolean SUCCESS = true;
    public static final boolean FAILURE = false;

    public static final String EMPTY_STRING = "";
    public static final String SPACE = " ";
    public static final String TAB = "\t";

    public static final String ALPHA_STRING_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String ALPHA_STRING_LOWERCASE = ALPHA_STRING_UPPERCASE.toLowerCase();
    public static final String NUMERIC_STRING = "0123456789";
    public static final String ALPHANUMERIC_STRING = ALPHA_STRING_UPPERCASE + ALPHA_STRING_LOWERCASE + NUMERIC_STRING;

}
