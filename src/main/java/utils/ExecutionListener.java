/*
 * Created by Yehor Karliuchenko on 11/28/18 1:48 PM
 */

package utils;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class ExecutionListener extends TestListenerAdapter {

    @Override
    public void onTestFailure(ITestResult result) {
        Log.info("[FAILED]: " + result.getMethod().getMethodName() + "\n" + result.getThrowable().getMessage() + "\n");
        DriverUtils.captureScreenshot(DriverUtils.getDriverInstance(result));
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        Log.info("[PASSED]: " + result.getMethod().getMethodName());
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        Log.info("[SKIPPED]: " + result.getMethod().getMethodName());
    }
}
