/*
 * Created by Yehor Karliuchenko on 11/22/18 6:32 PM
 */

package utils;

import io.qameta.allure.Attachment;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

public final class DriverUtils {

    private DriverUtils() {
    }

    public static WebDriverWait initWaits(WebDriver driver, int iWait, int eWait, long pollingTime) {
        driver.manage().timeouts().implicitlyWait(iWait, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, eWait);
        wait.pollingEvery(Duration.ofSeconds(pollingTime));
        return wait;
    }

    public static void captureScreenshot(WebDriver driver, String filePath, String fileName) {
        driver = new Augmenter().augment(driver);
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(srcFile, new File(filePath +  fileName + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void captureScreenshot(WebDriver driver){
        driver = new Augmenter().augment(driver);
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(srcFile, new File(Consts.SCREENSHOT_OUT_DIR, Util.getCurrentLocalDateTimeStamp() + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static WebDriver getDriverInstance(ITestResult result) {
        Class clazz = result.getTestClass().getRealClass();
        Field field = null;
        try {
            field = clazz.getDeclaredField("driver");
        } catch (NoSuchFieldException ex) {
            ex.printStackTrace();
            Log.error("No 'driver' instance declared");
        }
        WebDriver driver = null;
        if (field != null) {
            field.setAccessible(true);
            try {
                driver = (WebDriver) field.get(result.getInstance());
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }
        }

        return driver;
    }

    @Attachment
    public static byte[] attachScreenshot(WebDriver driver){
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}
