/*
 * Created by Yehor Karliuchenko on 11/26/18 7:54 PM
 */

package utils;

public final class Vars {
    private Vars() {
    }

    private static final String DEFAULT_UI_ENV = "dev";
    public static final String DEFAULT_WSU_USERNAME = "keytracerWSU";
    public static final String DEFAULT_WSU_PASSWORD = "S3cur3";

    private static final String[][] RTNHubEnvs = {
            {"http://10.27.100.30:8080", DEFAULT_WSU_USERNAME, DEFAULT_WSU_PASSWORD},     //dev
            {"http://10.27.100.31:8080", DEFAULT_WSU_USERNAME, DEFAULT_WSU_PASSWORD},     //devtest
    };

    public static final String RTNHUB_BASE_URL = RTNHubEnvs[getUiEnvIndex()][0];
    public static final String WSU_USERNAME = RTNHubEnvs[getUiEnvIndex()][1];
    public static final String WSU_PASSWORD = RTNHubEnvs[getUiEnvIndex()][2];

    public static int getUiEnvIndex() {
        switch (System.getProperty("ui_env", DEFAULT_UI_ENV)) {
            case "dev":
                return 0;
            case "devtest":
                return 1;
            default:
                return 0;
        }
    }


}
