/*
 * Created by Yehor Karliuchenko on 11/23/18 12:59 PM
 */

package ui.rtnhub.widgets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.pages.*;

public class NavSideBar extends BasePage {

    @FindBy(css = ".topbar-right #menu-button") private WebElement btnSideBarArrow;
    @FindBy(css = "menuform:org") private WebElement lblCompany;

    @FindBy(css = "#menuform\\:um_users a") private WebElement linkUsers;

    @FindBy(css = "#menuform\\:um_assets") private WebElement treAssets;
    @FindBy(css = "#menuform\\:um_keytags > a") private WebElement linkKeyTags;
    @FindBy(css = "#menuform\\:um_keytagGroup > a") private WebElement linkKeyTagGroups;
    @FindBy(css = "#menuform\\:um_item > a") private WebElement linkItems;
    @FindBy(css = "#menuform\\:um_itemGroups > a") private WebElement linkItemGroups;
    @FindBy(css = "#menuform\\:um_terminal > a") private WebElement linkSmartTerminal;

    @FindBy(css = "#menuform\\:um_reservation") private WebElement treReservations;
    @FindBy(css = "#menuform\\:um_reservation_calendar > a") private WebElement linkCalendarView;

    @FindBy(css = "#menuform\\:um_report_prototype > a") private WebElement linkReports;

    @FindBy(css = "#menuform\\:um_sm1") private WebElement treModules;
    @FindBy(css = "#menuform\\:um_ldap > a") private WebElement linkLDAP;
    @FindBy(css = "#menuform\\:um_email_server > a") private WebElement linkEmailServer;

    @FindBy(css = "#menuform\\:um_sm2") private WebElement treIntegration;
    @FindBy(css = "#menuform\\:um_s2 > a") private WebElement linkS2Netbox;

    @FindBy(css = "#menuform\\:um_sm5") private WebElement treLanguage;
    @FindBy(css = "#menuform\\:um_lnk511 > a") private WebElement linkEnglish;
    @FindBy(css = "#menuform\\:um_lnk512 > a") private WebElement linkFrench;

    @FindBy(css = "#menuform\\:um_admin") private WebElement treAdmin;
    @FindBy(css = "#menuform\\:um_admin_data > a") private WebElement linkAdminData;
    @FindBy(css = "#menuform\\:um_adminSetting > a") private WebElement linkAdminSettings;
    @FindBy(css = "#menuform\\:um_attributeList > a") private WebElement linkAttributeList;
    @FindBy(css = "#menuform\\:um_attributeDefin > a") private WebElement linkAttributeDefinition;

    public NavSideBar(WebDriver driver) {
        super(driver);
    }

    @Override
    public NavSideBar ensurePageLoaded() {
        return this;
    }

    public boolean isSideBarExpanded(){
        return !waitForVisible(btnSideBarArrow).getAttribute("class").equals("menu-button-rotate");
    }

    public NavSideBar expandSideBar(){
        if(!isSideBarExpanded()){
            clickWhenReady(btnSideBarArrow);
        }
        return this;
    }

    public NavSideBar collapseSideBar(){
        if(isSideBarExpanded()){
            clickWhenReady(btnSideBarArrow);
        }
        return this;
    }

    private NavSideBar expandAssets() {
        String styleAttribute = waitForVisible(treAssets).findElement(By.cssSelector("ul[role='menu']")).getAttribute("style");
        if (styleAttribute == null || styleAttribute.isEmpty() || styleAttribute.contains("display: none;")) {
            clickWhenReady(treAssets);
        }
        return this;
    }

    private NavSideBar expandReservations() {
        String styleAttribute = waitForVisible(treReservations).findElement(By.cssSelector("ul[role='menu']")).getAttribute("style");
        if (styleAttribute == null || styleAttribute.isEmpty() || styleAttribute.contains("display: none;")) {
            clickWhenReady(treReservations);
        }
        return this;
    }

    private NavSideBar expandModules() {
        String styleAttribute = waitForVisible(treModules).findElement(By.cssSelector("ul[role='menu']")).getAttribute("style");
        if (styleAttribute == null || styleAttribute.isEmpty() || styleAttribute.contains("display: none;")) {
            clickWhenReady(treModules);
        }
        return this;
    }

    private NavSideBar expandIntegration() {
        String styleAttribute = waitForVisible(treIntegration).findElement(By.cssSelector("ul[role='menu']")).getAttribute("style");
        if (styleAttribute == null || styleAttribute.isEmpty() || styleAttribute.contains("display: none;")) {
            clickWhenReady(treIntegration);
        }
        return this;
    }

    private NavSideBar expandLanguages() {
        String styleAttribute = waitForVisible(treLanguage).findElement(By.cssSelector("ul[role='menu']")).getAttribute("style");
        if (styleAttribute == null || styleAttribute.isEmpty() || styleAttribute.contains("display: none;")) {
            clickWhenReady(treLanguage);
        }
        return this;
    }

    private NavSideBar expandAdmin() {
        String styleAttribute = waitForVisible(treAdmin).findElement(By.cssSelector("ul[role='menu']")).getAttribute("style");
        if (styleAttribute == null || styleAttribute.isEmpty() || styleAttribute.contains("display: none;")) {
            clickWhenReady(treAdmin);
        }
        return this;
    }

    public UsersPage openUsers() {
        clickWhenReady(linkUsers);
        return new UsersPage(driver).ensurePageLoaded();
    }

    public KeyTagsPage openKeyTags() {
        expandAssets().clickWhenReady(linkKeyTags);
        return new KeyTagsPage(driver).ensurePageLoaded();
    }

    public KeyTagGroupsPage openKeyTagGroups() {
        expandAssets().clickWhenReady(linkKeyTagGroups);
        return new KeyTagGroupsPage(driver).ensurePageLoaded();
    }

    public ItemsPage openItems() {
        expandAssets().clickWhenReady(linkItems);
        return new ItemsPage(driver).ensurePageLoaded();
    }

    public ItemGroupsPage openItemGroups() {
        expandAssets().clickWhenReady(linkItemGroups);
        return new ItemGroupsPage(driver).ensurePageLoaded();
    }

    public SmartTerminalPage openSmartTerminal() {
        expandAssets().clickWhenReady(linkSmartTerminal);
        return new SmartTerminalPage(driver).ensurePageLoaded();
    }

    public ReservationsCalendarPage openCalendarReservations() {
        expandReservations().clickWhenReady(linkCalendarView);
        return new ReservationsCalendarPage(driver).ensurePageLoaded();
    }

    public ReportsPage openReports() {
        clickWhenReady(linkReports);
        return new ReportsPage(driver).ensurePageLoaded();
    }

    public LdapPage openLdap() {
        expandModules().clickWhenReady(linkLDAP);
        return new LdapPage(driver).ensurePageLoaded();
    }

    public EmailServerPage openEmailServer() {
        expandModules().clickWhenReady(linkEmailServer);
        return new EmailServerPage(driver).ensurePageLoaded();
    }

    public S2NetboxPage openS2Netbox() {
        expandIntegration().clickWhenReady(linkS2Netbox);
        return new S2NetboxPage(driver).ensurePageLoaded();
    }

    public AdminDataPage openAdminData() {
        expandAdmin().clickWhenReady(linkAdminData);
        return new AdminDataPage(driver).ensurePageLoaded();
    }

    public AdminSettingsPage openAdminSettings() {
        expandAdmin().clickWhenReady(linkAdminSettings);
        return new AdminSettingsPage(driver).ensurePageLoaded();
    }

    public AttributeListPage openAttributeList() {
        expandAdmin().clickWhenReady(linkAttributeList);
        return new AttributeListPage(driver).ensurePageLoaded();
    }

    public AttributeDefinitionPage openAttributeDefinition() {
        expandAdmin().clickWhenReady(linkAttributeDefinition);
        return new AttributeDefinitionPage(driver).ensurePageLoaded();
    }
}
