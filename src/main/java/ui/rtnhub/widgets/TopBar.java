/*
 * Created by Yehor Karliuchenko on 11/23/18 6:55 PM
 */

package ui.rtnhub.widgets;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.pages.LoginPage;

public class TopBar extends BasePage {

    @FindBy(css = ".logo") private WebElement imgLogo;
    @FindBy(css = ".topbar-right #menu-button") private WebElement btnChevron;
    @FindBy(css = ".topbar-right > span:first-of-type") private WebElement lblCompanyName;
    @FindBy(css = ".topbar-right > span:nth-of-type(2)") private WebElement lblUserName;
    @FindBy(css = ".profile-image") private WebElement imgProfileImage;
    @FindBy(css = "#logoutform div") private WebElement linkLogOut;

    public TopBar(WebDriver driver) {
        super(driver);
    }

    @Override
    public TopBar ensurePageLoaded() {
        waitForVisible(imgLogo);
        waitForVisible(btnChevron);
        waitForVisible(lblCompanyName);
        waitForVisible(lblUserName);
        waitForVisible(imgProfileImage);
        return this;
    }

    public TopBar expandUserDropDown() {
        clickWhenReady(imgProfileImage);
        return this;
    }

    public LoginPage logOut() {
        expandUserDropDown().clickWhenReady(linkLogOut);
        return new LoginPage(driver);
    }

    public String getCompanyLabel() {
        return waitForVisible(lblCompanyName).getText();
    }

    public String getUserName() {
        return waitForVisible(lblUserName).getText();
    }
}
