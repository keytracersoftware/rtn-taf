/*
 * Created by Yehor Karliuchenko on 11/23/18 5:44 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

import static org.testng.AssertJUnit.assertTrue;

public class HomePage extends BasePage {

    @FindBy(css = "a[href*='RTN-KeyTracer']") private WebElement linkKeyTracer;
    @FindBy(css = "a[href*='RTN-AssetTracer']") private WebElement linkAssetTracer;
    @FindBy(css = "a[href*='real-time-location-solutions-keys-assets-people']") private WebElement linkRTNMobile;
    @FindBy(css = "img[src*='logo-keytracer.jpg']") private WebElement imgKeytracerLogo;
    @FindBy(css = "img[src*='logo-assettracer.jpg']") private WebElement imgAssetTracerLogo;
    @FindBy(css = "img[src*='logo-rtnmobile.jpg']") private WebElement imgRTNMobileLogo;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public HomePage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the Home page is loaded by checking the following items to be visible: " +
            "KeyTracer, AssetTracer and RTNmobile links and logos")
    public HomePage ensurePageLoaded() {
        waitForVisible(linkAssetTracer);
        waitForVisible(linkKeyTracer);
        waitForVisible(linkRTNMobile);
        assertTrue("KeyTracer logo is not retrieved to view", isImageLoaded(imgKeytracerLogo));
        assertTrue("AssetTracer logo is not retrieved to view", isImageLoaded(imgAssetTracerLogo));
        assertTrue("RTNMobile logo is not retrieved to view", isImageLoaded(imgRTNMobileLogo));
        return this;
    }
}
