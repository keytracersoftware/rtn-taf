/*
 * Created by Yehor Karliuchenko on 11/23/18 8:00 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class AdminSettingsPage extends BasePage {

    @FindBy(css = "#form\\:showUsername span") private WebElement chbShowTerminalDisplayName;
    @FindBy(css = "#form\\:isStanderTime span") private WebElement chbShowStandartTime;
    @FindBy(css = "#form\\:showOwner span") private WebElement chbShowReservationOwner;
    @FindBy(xpath = "//td[text()='Reservation Key Limit']/following-sibling::td//select") private WebElement ddReservationKeyLimit;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public AdminSettingsPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'Admin Settings' page is loaded by checking the following items to be visible: " +
            "'Show Terminal Display Name' & ''Show Reservation Owner' & ''Show Standard Time' checkboxes, " +
            "'Reservation Key Limit' dropdown")
    public AdminSettingsPage ensurePageLoaded() {
        waitForVisible(chbShowTerminalDisplayName);
        waitForVisible(chbShowStandartTime);
        waitForVisible(chbShowReservationOwner);
        waitForVisible(ddReservationKeyLimit);
        return this;
    }
}
