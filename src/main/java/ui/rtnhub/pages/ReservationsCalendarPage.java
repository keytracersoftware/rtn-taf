/*
 * Created by Yehor Karliuchenko on 11/23/18 7:58 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class ReservationsCalendarPage extends BasePage {

    @FindBy(css = "#reservationForm") private WebElement formReservation;
    @FindBy(css = "#reservationForm\\:reservationDetails") private WebElement formReservationDetails;
    @FindBy(css = "#reservationForm\\:schedule") private WebElement pnlCalendarSchedule;
    @FindBy(css = "#reservationForm\\:addButton") private WebElement btnAddReservation;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public ReservationsCalendarPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'Calendar View' page is loaded by checking the following items to be visible: " +
            "Reservation form, Reservation creation form, Calendar, Add Reservation button")
    public ReservationsCalendarPage ensurePageLoaded() {
        waitForVisible(formReservation);
        waitForVisible(formReservationDetails);
        waitForVisible(pnlCalendarSchedule);
        waitForVisible(btnAddReservation);
        return this;
    }
}
