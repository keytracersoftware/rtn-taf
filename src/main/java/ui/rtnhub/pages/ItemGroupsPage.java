/*
 * Created by Yehor Karliuchenko on 11/23/18 7:57 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class ItemGroupsPage extends BasePage {

    @FindBy(css = "#form\\:itemGroupsTable_head") private WebElement headItemGroupsTable;
    @FindBy(css = "#form\\:itemGroupsTable_data") private WebElement tblItemGroups;
    @FindBy(xpath = "//div[@id='form:itemGroupsTable']//span[text()='Add']/..") private WebElement btnAddItemGroup;
    @FindBy(css = "#form\\:itemGroupsTable\\:editButton") private WebElement btnEditItemGroup;
    @FindBy(css = "#form\\:itemGroupsTable\\:deleteButton") private WebElement btnDeleteItemGroup;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public ItemGroupsPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'Item Groups' page is loaded by checking the following items to be visible: " +
            "Item Group table body & head, 'Add', 'Edit', & 'Delete' buttons")
    public ItemGroupsPage ensurePageLoaded() {
        waitForVisible(headItemGroupsTable);
        waitForVisible(tblItemGroups);
        waitForVisible(btnAddItemGroup);
        waitForVisible(btnEditItemGroup);
        waitForVisible(btnDeleteItemGroup);
        return this;
    }
}
