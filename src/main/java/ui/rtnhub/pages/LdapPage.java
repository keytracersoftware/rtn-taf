/*
 * Created by Yehor Karliuchenko on 11/23/18 7:59 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class LdapPage extends BasePage {

    @FindBy(css = ".card form[action='/secure/admin/ldap.xhtml']") private WebElement formLdap;
    @FindBy(css = "div[id*='LDAPSettings_content'] input[id*='url']") private WebElement txtLdapUrl;
    @FindBy(css = "div[id*='LDAPSettings_content'] input[id*='basedn']") private WebElement txtBaseDn;
    @FindBy(css = "div[id*='LDAPSettings_content'] input[id*='groupSearchBase']") private WebElement txtUserSearchBaseDn;
    @FindBy(css = "div[id*='LDAPSettings_content'] input[id*='user']") private WebElement txtUserName;
    @FindBy(css = "div[id*='LDAPSettings_content'] input[id*='pass']") private WebElement txtPassword;
    @FindBy(css = "div[id*='LDAPSettings_content'] input[id*='filter']") private WebElement txtFilter;
    @FindBy(css = "div[id*='LDAPSettings_content'] div[id*='status']") private WebElement chbStatus;
    @FindBy(css = "div[id*='LDAPSettings_content'] div[id*='sso']") private WebElement chbSSO;
    @FindBy(xpath = "//span[text()='Save Settings']/..") private WebElement btnSaveSettings;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public LdapPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'LDAP' page is loaded by checking the following items to be visible: " +
            "'LDAP Server URL', 'Base DN', 'User Search Base DN', 'Username', 'Password', " +
            "'Filter(User Identifier)' fields, 'Enable' & 'SSO Enable' checkboxes'")
    public LdapPage ensurePageLoaded() {
        waitForVisible(formLdap);
        waitForVisible(txtLdapUrl);
        waitForVisible(txtBaseDn);
        waitForVisible(txtUserSearchBaseDn);
        waitForVisible(txtUserName);
        waitForVisible(txtPassword);
        waitForVisible(txtFilter);
        waitForVisible(chbStatus);
        waitForVisible(chbSSO);
        waitForVisible(btnSaveSettings);
        return this;
    }
}
