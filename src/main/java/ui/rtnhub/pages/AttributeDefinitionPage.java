/*
 * Created by Yehor Karliuchenko on 11/23/18 8:00 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class AttributeDefinitionPage extends BasePage {

    @FindBy(css = "#form\\:entityType_input") private WebElement ddEntityType;
    @FindBy(css = "#form\\:attributesTable_head") private WebElement headAttributesTable;
    @FindBy(css = "#form\\:attributesTable_data") private WebElement tblAttributes;
    @FindBy(xpath = "//span[text() ='Update']/..") private WebElement btnUpdate;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public AttributeDefinitionPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'Attribute Definition' page is loaded by checking the following items to be visible: 'Entity Type' dropdown, " +
            "Attributes table head & body, 'Update' button")
    public AttributeDefinitionPage ensurePageLoaded() {
        waitForVisible(headAttributesTable);
        waitForVisible(tblAttributes);
        waitForVisible(btnUpdate);
        return this;
    }
}
