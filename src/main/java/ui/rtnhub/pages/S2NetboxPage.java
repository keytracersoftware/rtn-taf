/*
 * Created by Yehor Karliuchenko on 11/23/18 7:59 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class S2NetboxPage extends BasePage {

    @FindBy(css = ".card form[action='/secure/admin/integration/netbox.xhtml']") private WebElement formS2Netbox;
    @FindBy(css = "div[id*='netboxSettings'] input[id*='serverAddress']") private WebElement txtS2ServerIpAddress;
    @FindBy(css = "div[id*='netboxSettings'] input[id*='serverUsername']") private WebElement txtS2ApiUsername;
    @FindBy(css = "div[id*='netboxSettings'] input[id*='serverPassword']") private WebElement txtS2ApiPassword;
    @FindBy(css = "button[id*='next']") private WebElement btnNext;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public S2NetboxPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'S2 Netbox' page is loaded by checking the following items to be visible: " +
            "S2 Secutiry wizard form, 'S2 VM Server IP Address', 'S2 API Username' & 'S2 API Password' fields, 'Next' button")
    public S2NetboxPage ensurePageLoaded() {
        waitForVisible(formS2Netbox);
        waitForVisible(txtS2ServerIpAddress);
        waitForVisible(txtS2ApiUsername);
        waitForVisible(txtS2ApiPassword);
        waitForVisible(btnNext);
        return this;
    }
}
