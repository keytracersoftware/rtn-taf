/*
 * Created by Yehor Karliuchenko on 11/23/18 7:57 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class KeyTagGroupsPage extends BasePage {

    @FindBy(css = "#form\\:keytagGroupTable_data") private WebElement tblKeyTagGroup;
    @FindBy(css = "#form\\:keytagGroupTable_head") private WebElement headKeyTagGroupTable;
    @FindBy(css = "#form\\:keytagGroupTable\\:keyTagButton") private WebElement btnViewKeyTagAssignments;
    @FindBy(css = "#form\\:keytagGroupTable\\:userButton") private WebElement btnViewUserAssignments;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public KeyTagGroupsPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'KeyTag Groups' page is loaded by checking the following items to be visible: " +
            "KeyTag Groups table body & head, 'View KeyTag Assignments' & 'View User Assignments' buttons")
    public KeyTagGroupsPage ensurePageLoaded() {
        waitForVisible(tblKeyTagGroup);
        waitForVisible(headKeyTagGroupTable);
        waitForVisible(btnViewKeyTagAssignments);
        waitForVisible(btnViewUserAssignments);
        return this;
    }
}
