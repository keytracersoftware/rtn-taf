/*
 * Created by Yehor Karliuchenko on 11/23/18 8:00 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class AdminDataPage extends BasePage {

    @FindBy(xpath = "//td[text() = 'ALL DATA:']/following-sibling::td/button") private WebElement btnImportAllData;
    @FindBy(xpath = "//td[text() = 'Users:']/following-sibling::td/button") private WebElement btnImportUsers;
    @FindBy(xpath = "//td[text() = 'KeyTags:']/following-sibling::td/button") private WebElement btnImportKeyTags;
    @FindBy(xpath = "//td[text() = 'Faults Codes:']/following-sibling::td/button") private WebElement btnImportFaultCodes;
    @FindBy(xpath = "//td[text() = 'Items:']/following-sibling::td/button") private WebElement btnImportItems;
    @FindBy(xpath = "//td[text() = 'KeyTags Assignment:']/following-sibling::td/button") private WebElement btnImportKeyTagAssignments;
    @FindBy(xpath = "//td[text() = 'Cabinets:']/following-sibling::td/button") private WebElement btnImportCabinets;
    @FindBy(xpath = "//td[text() = 'Cabinet KeyTag Assignments:']/following-sibling::td/button") private WebElement btnImportCabinetKeyTagAssignments;
    @FindBy(xpath = "//td[text() = 'Terminals:']/following-sibling::td/button") private WebElement btnImportTerminals;
    @FindBy(xpath = "//td[text() = 'Terminal Cabinet Assignments:']/following-sibling::td/button") private WebElement btnImportTerminalCabinetAssignments;
    @FindBy(xpath = "//td[text() = 'Credentials:']/following-sibling::td/button") private WebElement btnImportCredentials;
    @FindBy(xpath = "//td[text() = 'User Rights Type']/following-sibling::td/button") private WebElement btnImportUserRightsType;
    @FindBy(xpath = "//td[text() = 'User Rights CRUD']/following-sibling::td/button") private WebElement btnImportUserRightsCrud;
    @FindBy(xpath = "//span[.='Upload Logs']/..") private WebElement btnUploadLogs;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public AdminDataPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'Admin Data' page is loaded by checking the following items to be visible: the Import buttons, 'Upload Logs' button")
    public AdminDataPage ensurePageLoaded() {
        waitForVisible(btnImportAllData);
        waitForVisible(btnImportUsers);
        waitForVisible(btnImportKeyTags);
        waitForVisible(btnImportFaultCodes);
        waitForVisible(btnImportItems);
        waitForVisible(btnImportKeyTagAssignments);
        waitForVisible(btnImportCabinets);
        waitForVisible(btnImportCabinetKeyTagAssignments);
        waitForVisible(btnImportTerminals);
        waitForVisible(btnImportTerminalCabinetAssignments);
        waitForVisible(btnImportCredentials);
        waitForVisible(btnImportUserRightsType);
        waitForVisible(btnImportUserRightsCrud);
        waitForVisible(btnUploadLogs);
        return this;
    }
}
