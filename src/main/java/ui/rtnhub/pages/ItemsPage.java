/*
 * Created by Yehor Karliuchenko on 11/23/18 7:57 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class ItemsPage extends BasePage {

    @FindBy(css = "#form\\:entityType_input") private WebElement ddItemType;
    @FindBy(css = "#form\\:itemsTable_head") private WebElement headItemManagementTable;
    @FindBy(css = "#form\\:itemsTable_data") private WebElement tblItemManagement;
    @FindBy(xpath = "//div[@id='form:itemsTable']//button[1]") private WebElement btnAddItem;
    @FindBy(css = "#form\\:itemsTable\\:editButton") private WebElement btnEditItem;
    @FindBy(css = "#form\\:itemsTable\\:deleteButton") private WebElement btnDeleteItem;
    
    public NavSideBar navSideBar;
    public TopBar topBar;

    public ItemsPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'Items' page is loaded by checking the following items to be visible: " +
            "Item table body & head, 'Add', 'Edit', & 'Delete' buttons, 'Item Type dropdown'")
    public ItemsPage ensurePageLoaded() {
        waitForVisible(ddItemType);
        waitForVisible(headItemManagementTable);
        waitForVisible(tblItemManagement);
        waitForVisible(btnAddItem);
        waitForVisible(btnEditItem);
        waitForVisible(btnDeleteItem);
        return this;
    }
}
