/*
 * Created by Yehor Karliuchenko on 11/23/18 7:58 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class SmartTerminalPage extends BasePage {

    @FindBy(css = "#form\\:terminalsTable_head") private WebElement headSmartTerminalTable;
    @FindBy(css = "#form\\:terminalsTable_data") private WebElement tblSmartTerminal;
    @FindBy(xpath = "//span[text()='Add']/..") private WebElement btnAddSmartTerminal;
    @FindBy(css = "#form\\:terminalsTable\\:editButton") private WebElement btnEditSmartTerminal;
    @FindBy(css = "#form\\:terminalsTable\\:deleteButton") private WebElement btnDeleteSmartTerminal;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public SmartTerminalPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'SmartTerminal' page is loaded by checking the following items to be visible: " +
            "SmartTerminal Management table head & body, 'Add', 'Edit' & 'Delete' buttons")
    public SmartTerminalPage ensurePageLoaded() {
        waitForVisible(headSmartTerminalTable);
        waitForVisible(tblSmartTerminal);
        waitForVisible(btnAddSmartTerminal);
        waitForVisible(btnEditSmartTerminal);
        waitForVisible(btnDeleteSmartTerminal);
        return this;
    }
}
