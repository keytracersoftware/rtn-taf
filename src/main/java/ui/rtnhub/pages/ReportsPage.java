/*
 * Created by Yehor Karliuchenko on 11/23/18 7:58 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class ReportsPage extends BasePage {

    @FindBy(css = "#form\\:report") private WebElement formReport;
    @FindBy(css = "#form\\:report_paginator_top") private WebElement tlbPagination;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public ReportsPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'Reports' page is loaded by checking the following items to be visible: " +
            "General Report form, Top Pagination")
    public ReportsPage ensurePageLoaded() {
        waitForVisible(formReport);
        waitForVisible(tlbPagination);
        return this;
    }
}
