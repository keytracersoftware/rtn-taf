/*
 * Created by Yehor Karliuchenko on 11/23/18 12:52 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;

import static org.testng.AssertJUnit.assertTrue;

public class LoginPage extends BasePage {
    
    @FindBy(css = "#loginform") private WebElement formLogin;
    @FindBy(css = "#app_username") private WebElement inpUserName;
    @FindBy(css = "#app_password") private WebElement inpPassword;
    @FindBy(css = "#loginform button[type='submit']") private WebElement btnSignIn;
    @FindBy(css = ".login-panel img[src*='logo-rtnhub.png']") private WebElement imgLogo;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    @Step("Making sure the 'Login' page is loaded by checking the following items to be visible: " +
            "Login form, 'Email' & 'Password' fields, 'Sign In' button")
    public LoginPage ensurePageLoaded() {
        waitForVisible(formLogin);
        waitForVisible(inpUserName);
        waitForVisible(inpPassword);
        waitForVisible(btnSignIn);
        return this;
    }

    @Step("Entering account credentials: {email} / {password}")
    public LoginPage enterCredentials(String email, String password){
        typeIn(inpUserName, email);
        typeIn(inpPassword, password);
        return this;
    }

    @Step("Clicking Sign In button")
    public LoginPage clickSignIn(){
        clickWhenReady(btnSignIn);
        return this;
    }

    @Step("Signing into RTNHub with: (email) / {password}")
    public HomePage signIn(String email, String password){
        enterCredentials(email, password);
        clickSignIn();
        return new HomePage(driver).ensurePageLoaded();
    }

}
