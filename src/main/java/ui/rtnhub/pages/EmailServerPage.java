/*
 * Created by Yehor Karliuchenko on 11/23/18 7:59 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class EmailServerPage extends BasePage {

    @FindBy(css = "#form\\:emailServerSettings") private WebElement formEmailSettings;
    @FindBy(css = "#form\\:replyAddress") private WebElement txtReplyAddress;
    @FindBy(css = "#form\\:hostname") private WebElement txtMailServerHost;
    @FindBy(css = "#form\\:port_input") private WebElement txtPort;
    @FindBy(css = "#form\\:securityDropDown_input") private WebElement ddConnectionSecurity;
    @FindBy(css = "#form\\:authRequired span") private WebElement chbAuthRequired;
    @FindBy(css = "#form\\:loginName") private WebElement txtLoginName;
    @FindBy(css = "#form\\:password") private WebElement txtPassword;
    @FindBy(css = "#form\\:newEmail button") private WebElement btnAddRecipient;
    @FindBy(xpath = "//span[text()='Save Settings']/..") private WebElement btnSaveSettings;

    public NavSideBar navSideBar;
    public TopBar topBar;

    public EmailServerPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'Email Server' page is loaded by checking the following items to be visible: Email Settings form, " +
            "'Reply Address', 'MailServer Hostname', 'Port', 'Connection Security', 'Login name', 'Password', 'Recipients' fields, " +
            "'Auth required' checkbox, '''Save Settings' button")
    public EmailServerPage ensurePageLoaded() {
        waitForVisible(formEmailSettings);
        waitForVisible(txtReplyAddress);
        waitForVisible(txtMailServerHost);
        waitForVisible(txtPort);
        waitForVisible(ddConnectionSecurity);
        waitForVisible(chbAuthRequired);
        waitForVisible(txtLoginName);
        waitForVisible(txtPassword);
        waitForVisible(btnAddRecipient);
        waitForVisible(btnSaveSettings);
        return this;
    }
}
