/*
 * Created by Yehor Karliuchenko on 11/23/18 8:00 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class AttributeListPage extends BasePage {

    @FindBy(css = "#form\\:entityType_input") private WebElement ddEntityType;
    
    public NavSideBar navSideBar;
    public TopBar topBar;

    public AttributeListPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'Attribute List' page is loaded by checking the following items to be visible: 'Entity Type' dropdown")
    public AttributeListPage ensurePageLoaded() {
        waitForVisible(ddEntityType);
        return this;
    }
}
