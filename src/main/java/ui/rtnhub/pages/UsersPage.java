/*
 * Created by Yehor Karliuchenko on 11/23/18 7:57 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

import java.util.List;

import static org.testng.AssertJUnit.assertTrue;

public class UsersPage extends BasePage {

    @FindBy(css = "#form\\:usersTable") private WebElement formUserTable;
    @FindBy(css = "#form\\:usersTable\\:globalFilter") private WebElement txtUserSearch;
    @FindBy(xpath = "//span[text()='Add User']/..") private WebElement btnAddUser;
    @FindBy(css = "#form\\:usersTable\\:editButton") private WebElement btnEdit;
    @FindBy(css = "#form\\:usersTable\\:deleteButton") private WebElement btnDeleteUser;
    @FindBy(css = "#form\\:usersTable\\:credentialButton") private WebElement btnCredentials;
    @FindBy(css = "#form\\:usersTable\\:itemButton") private WebElement btnItemAssignments;

    @FindBy(css = "#form\\:usersTable_data tr") private List<WebElement> dgdUserTableRows;
    private By terminalDisplayName = By.cssSelector("td:nth-child(1)");
    private By userName = By.cssSelector("td:nth-child(2)");
    private By firstName = By.cssSelector("td:nth-child(3)");
    private By lastName = By.cssSelector("td:nth-child(4)");
    private By userType = By.cssSelector("td:nth-child(5)");
    private By status = By.cssSelector("td:nth-child(6) span");

    @FindBy(css = "#dialogForm\\:firstName") private WebElement txtFirstName;
    @FindBy(css = "#dialogForm\\:lastName") private WebElement txtLastName;
    @FindBy(css = "#dialogForm\\:userCategory span[class*='ui-icon-triangle']") private WebElement btnUserCategories;
    @FindBy(css = "#dialogForm\\:active span") private WebElement chbActive;

    @FindBy(css = "#dialogForm\\:userCategory_panel li[data-item-value='1'] span") private WebElement chbTerminalUser;
    @FindBy(css = "#dialogForm\\:userCategory_panel li[data-item-value='2'] span") private WebElement chbWebUser;
    @FindBy(css = "#dialogForm\\:userCategory_panel li[data-item-value='3'] span") private WebElement chbRFIDUser;
    @FindBy(css = "#dialogForm\\:userCategory li[data-item-value='1'] span[class*='token-label']") private WebElement lblTerminalUser;
    @FindBy(css = "#dialogForm\\:userCategory li[data-item-value='2'] span[class*='token-label']") private WebElement lblWebUser;
    @FindBy(css = "#dialogForm\\:userCategory li[data-item-value='3'] span[class*='token-label']") private WebElement lblRFIDUser;
    @FindBy(css = "#dialogForm\\:userCategory_panel a[aria-label='Close']") private WebElement btnCloseCategories;

    @FindBy(css = "#dialogForm\\:terminalDisplayName") private WebElement txtTerminalDisplayName;

    @FindBy(css = "#dialogForm\\:userType_input") private WebElement ddUserType;
    @FindBy(css = "#dialogForm\\:userName") private WebElement txtUsername;
    @FindBy(css = "#dialogForm\\:password") private WebElement txtPassword;
    @FindBy(css = "#dialogForm\\:resetPassword span") private WebElement chbResetPwdNextLogin;

    @FindBy(xpath = "//form[@id='dialogForm']//span[text()='Save']/..") private WebElement btnSave;
    @FindBy(xpath = "//form[@id='dialogForm']//span[text()='Cancel']/..") private WebElement btnCancel;


    public NavSideBar navSideBar;
    public TopBar topBar;

    public UsersPage(WebDriver driver){
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    public enum UserCategory {
        TERMINAL_USER,
        WEB_USER,
        RFID_USER;
    }

    public enum UserType {
        ADMIN {
            @Override
            public String toString(){
                return "Admin";
            }
        },
        USER {
            @Override
            public String toString(){
                return "User";
            }
        },
        WORLD_SUPER_USER {
            @Override
            public String toString(){
                return "World Super User";
            }
        },
        SUPER_USER {
            @Override
            public String toString(){
                return "Super User";
            }
        }
    }

    @Override
    @Step("Making sure the 'Users' page is loaded by checking the following items to be visible: " +
            "Users form, 'Add', 'Edit', 'Delete', 'Credentials' & 'Item Assignments' buttons")
    public UsersPage ensurePageLoaded(){
        waitForVisible(formUserTable);
        waitForVisible(btnAddUser);
        waitForVisible(btnEdit);
        waitForVisible(btnDeleteUser);
        waitForVisible(btnCredentials);
        waitForVisible(btnItemAssignments);
        return this;
    }

    @Step("Creating a new user")
    public UsersPage createNewTerminalUser(String firstName, String lastName,
                                           boolean isActive, String terminalDisplayName){
        clickAddUser()
                .enterFirstName(firstName)
                .enterLastName(lastName)
                .setActive(isActive)
                .setUserCategory(UserCategory.TERMINAL_USER)
                .enterTerminalDisplayName(terminalDisplayName)
                .save()
                .verifyUserExistByTerminalName(terminalDisplayName);
        handleRtnHubSpinner();
        return this;
    }


    @Step("Searching for: {var}")
    public UsersPage searchFor(String var){
        handleRtnHubSpinner();
        typeIn(txtUserSearch, var);
        return this;
    }

    @Step()
    public UsersPage verifyUserExistByTerminalName(String terminalName){
        searchFor(terminalName);
        assertTrue("User with '" + terminalName + "' as terminal name doesn't exist",
                   waitForVisible(dgdUserTableRows).stream().anyMatch(
                           element -> element.findElement(terminalDisplayName).getText().contains(terminalName)));
        return this;
    }

    @Step("Clicking 'Add User' button")
    public UsersPage clickAddUser(){
        clickWhenReady(btnAddUser);
        return this;
    }

    @Step("Entering first name: {firstName}")
    public UsersPage enterFirstName(String firstName){
        typeIn(txtFirstName, firstName);
        return this;
    }

    @Step("Entering last name: {lastName}")
    public UsersPage enterLastName(String lastName){
        typeIn(txtLastName, lastName);
        return this;
    }

    @Step("Setting user 'active' status: {active}")
    public UsersPage setActive(boolean active){
        String chbClass = waitForVisible(chbActive).getAttribute("class");
        if(active && !chbClass.contains("ui-icon-check")) {
            clickWhenReady(chbActive);
        }
        if(!active && !chbClass.contains("ui-icon-blank")) {
            clickWhenReady(chbActive);
        }
        return this;
    }

    @Step("Expanding 'User Categories dropdown'")
    public UsersPage expandUserCategories(){
        clickWhenReady(btnUserCategories);
        return this;
    }

    @Step("Collapsing User Categories dropdown")
    public UsersPage collapseUserCategories(){
        clickWhenReady(btnCloseCategories);
        return this;
    }

    @Step("Setting User Category: {category}")
    public UsersPage setUserCategory(UserCategory category){
        expandUserCategories();
        if(category == UserCategory.TERMINAL_USER && !waitForVisible(chbTerminalUser).getAttribute("class").contains(
                "ui-icon-check")) {
            clickWhenReady(chbTerminalUser);
            waitForVisible(lblTerminalUser);
        }
        if(category == UserCategory.WEB_USER && !waitForVisible(chbWebUser).getAttribute("class").contains(
                "ui-icon-check")) {
            clickWhenReady(chbWebUser);
            waitForVisible(lblWebUser);
        }
        if(category == UserCategory.RFID_USER && !waitForVisible(chbRFIDUser).getAttribute("class").contains(
                "ui-icon-check")) {
            clickWhenReady(chbRFIDUser);
            waitForVisible(lblRFIDUser);
        }
        collapseUserCategories();
        return this;
    }

    @Step("Entering Terminal Display Name: {terminalName}")
    public UsersPage enterTerminalDisplayName(String terminalName){
        typeIn(txtTerminalDisplayName, terminalName);
        return this;
    }

    @Step("Selecting Web User Type: {type}")
    public UsersPage selectUserType(UserType type){
        if(type == UserType.ADMIN) selectByText(ddUserType, type.toString());
        if(type == UserType.USER) selectByText(ddUserType, type.toString());
        if(type == UserType.WORLD_SUPER_USER) selectByText(ddUserType, type.toString());
        if(type == UserType.SUPER_USER) selectByText(ddUserType, type.toString());
        return this;
    }

    @Step("Entering Web User username: {username}")
    public UsersPage enterUserName(String username){
        typeIn(txtUsername, username);
        return this;
    }

    @Step("Entering Web User password: {password}")
    public UsersPage enterPassword(String password){
        typeIn(txtPassword, password);
        return this;
    }

    @Step("Setting 'Reset Password on next login': (reset)")
    public UsersPage setPasswordResetOnNextLogin(boolean isSet){
        String chbClass = waitForVisible(chbResetPwdNextLogin).getAttribute("class");
        if(isSet && !chbClass.contains("ui-icon-check")) {
            clickWhenReady(chbResetPwdNextLogin);
        }
        if(!isSet && !chbClass.contains("ui-icon-blank")) {
            clickWhenReady(chbResetPwdNextLogin);
        }
        return this;
    }

    @Step("Clicking 'Save' button")
    public UsersPage save(){
        clickWhenReady(btnSave);
        return this;
    }

    @Step("Clicking 'Cancel' button")
    public UsersPage cancel(){
        clickWhenReady(btnSave);
        return this;
    }
}
