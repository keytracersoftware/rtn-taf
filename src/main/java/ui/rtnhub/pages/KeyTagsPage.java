/*
 * Created by Yehor Karliuchenko on 11/23/18 7:57 PM
 */

package ui.rtnhub.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ui.BasePage;
import ui.rtnhub.widgets.NavSideBar;
import ui.rtnhub.widgets.TopBar;

public class KeyTagsPage extends BasePage {

    @FindBy(css = "#form\\:keyTagTable_data") private WebElement tblKeyTag;
    @FindBy(css = "#form\\:keyTagTable_head") private WebElement headKeyTagTable;
    @FindBy(css = "#form\\:keyTagTable\\:editButton") private WebElement btnEditKeytag;
    //@FindBy(css = "#form\\:keyTagTable\\:deleteButton") private WebElement btnDeleteKeyTag;
    
    public NavSideBar navSideBar;
    public TopBar topBar;

    public KeyTagsPage(WebDriver driver) {
        super(driver);
        navSideBar = new NavSideBar(driver);
        topBar = new TopBar(driver);
    }

    @Override
    @Step("Making sure the 'KeyTags' page is loaded by checking the following items to be visible: " +
            "KeyTags table body & head, 'Edit', & 'Delete' buttons")
    public KeyTagsPage ensurePageLoaded() {
        waitForVisible(tblKeyTag);
        waitForVisible(headKeyTagTable);
        waitForVisible(btnEditKeytag);
        //waitForVisible(btnDeleteKeyTag);
        return this;
    }
}
