/*
 * Created by Yehor Karliuchenko on 11/22/18 7:07 PM
 */

package ui;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.DriverUtils;

import java.util.List;

//region WebElements naming convention:
/*---------+----------------------------+--------+-----------------+
| Category |      UI/Control type       | Prefix |     Example     |
+----------+----------------------------+--------+-----------------+
| Basic    | Button                     | btn    | btnExit         |
| Basic    | Check box                  | chb    | chbReadOnly     |
| Basic    | Combo box                  | cbo    | cboEnglish      |
| Basic    | Common dialog              | dlg    | dlgFileOpen     |
| Basic    | Counter                    | cntr   | cntrCartItems   |
| Basic    | Date picker                | dtp    | dtpPublished    |
| Basic    | Dropdown List / Select tag | dd     | ddlCountry      |
| Basic    | Form                       | form   | formEntry       |
| Basic    | Frame                      | fra    | fraLanguage     |
| Basic    | Head / Header              | head   | headTable       |
| Basic    | Image                      | img    | imgIcon         |
| Basic    | Input                      | inp    | inpUser         |
| Basic    | Label                      | lbl    | lblHelpMessage  |
| Basic    | Links/Anchor Tags          | link   | linkForgotPwd   |
| Basic    | List box                   | list   | listPolicyCodes |
| Basic    | Menu                       | menu   | menuFileOpen    |
| Basic    | Message Box                | msgb   | msgbWrongEmail  |
| Basic    | Radio button / group       | rdo    | rdoGender       |
| Basic    | RichTextBox                | rtf    | rtfReport       |
| Basic    | Table                      | tbl    | tblCustomer     |
| Basic    | TabStrip                   | tab    | tabOptions      |
| Basic    | Text Area                  | txa    | txaDescription  |
| Basic    | Text Box                   | txt    | txtLastName     |
| Complex  | Chevron                    | chv    | chvProtocol     |
| Complex  | Data grid                  | dgd    | dgdTitles       |
| Complex  | Data list                  | dbl    | dblPublisher    |
| Complex  | Directory list box         | dir    | dirSource       |
| Complex  | Drive list box             | drv    | drvTarget       |
| Complex  | File list box              | fil    | filSource       |
| Complex  | Panel/Fieldset             | pnl    | pnlGroup        |
| Complex  | ProgressBar                | prg    | prgLoadFile     |
| Complex  | Slider                     | sld    | sldScale        |
| Complex  | Spinner                    | spn    | spnPages        |
| Complex  | StatusBar                  | sta    | staDateTime     |
| Complex  | Timer                      | tmr    | tmrAlarm        |
| Complex  | Toolbar                    | tlb    | tlbActions      |
| Complex  | TreeView                   | tre    | treOrganization |
+----------+----------------------------+--------+-----------------+*/
//endregion

public abstract class BasePage {

    protected WebDriver driver;
    private final WebDriverWait wait;

    public BasePage(WebDriver driver){
        PageFactory.initElements(driver, this);
        this.driver = driver;
        wait = DriverUtils.initWaits(driver, 10, 30, 1);
    }

    public abstract BasePage ensurePageLoaded();

    public void handleRtnHubSpinner(){
        By ajaxSpinner = By.cssSelector("i[class*='ajax-loader']");
        try{
            wait.until(ExpectedConditions.invisibilityOfElementLocated(ajaxSpinner));
        } catch(NoSuchElementException ex){
            ex.printStackTrace();
        }
    }

    private void waitForJSToLoad(){
        wait.until(WebDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState")
                .equals("complete"));
    }

    private void waitForJQueryToLoad(){
        wait.until(WebDriver -> (Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
    }

    protected WebElement waitForVisible(WebElement element){
        waitForJSToLoad();
        wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    protected List<WebElement> waitForVisible(List<WebElement> elements){
        waitForJSToLoad();
        wait.until(ExpectedConditions.visibilityOfAllElements(elements));
        return elements;
    }

    protected WebElement waitForInvisible(WebElement element){
        waitForJSToLoad();
        wait.until(ExpectedConditions.invisibilityOf(element));
        return element;
    }

    protected WebElement clickWhenReady(WebElement element){
        waitForVisible(element);
        scrollIntoView(element);
        wait.until(ExpectedConditions.elementToBeClickable(element)).click();
        return element;
    }

    protected WebElement clickByAction(WebElement element){
        Actions actions = new Actions(driver);
        actions.moveToElement(waitForVisible(element)).click().build().perform();
        return element;
    }

    protected WebElement typeIn(WebElement element, String inputText){
        waitForVisible(element);
        element.clear();
        element.sendKeys(inputText);
        return element;
    }

    protected WebElement hoverOver(WebElement element){
        Actions actions = new Actions(driver);
        actions.moveToElement(element).build().perform();
        return element;
    }

    protected void dragAndDrop(WebElement sourceElement, WebElement destinationElement){
        Actions actions = new Actions(driver);
        actions.dragAndDrop(sourceElement, destinationElement)
                .build()
                .perform();
    }

    protected WebElement scrollIntoView(WebElement element){
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        return element;
    }

    protected void selectByText(WebElement dd, String optionText){
        Select select = new Select(waitForVisible(dd));
        select.selectByVisibleText(optionText);
    }

    protected void selectByValue(WebElement dd, String optionValue){
        Select select = new Select(waitForVisible(dd));
        select.selectByValue(optionValue);
    }

    protected List<WebElement> getSelectOptions(WebElement dd){
        Select select = new Select(waitForVisible(dd));
        return select.getOptions();
    }

    protected boolean isAtribtuePresent(WebElement element, String attribute){
        return element.getAttribute(attribute) != null;
    }

    protected WebElement jsClick(WebElement element){
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", waitForVisible(element));
        return element;
    }

    protected Boolean isImageLoaded(WebElement img){
        Object result = ((JavascriptExecutor) driver).executeScript(
                "return arguments[0].complete && " +
                        "typeof arguments[0].naturalWidth != \"undefined\" && " +
                        "arguments[0].naturalWidth > 0", waitForVisible(img));

        boolean loaded = false;
        if(result instanceof Boolean) {
            loaded = (Boolean) result;
        }
        return loaded;
    }
}
