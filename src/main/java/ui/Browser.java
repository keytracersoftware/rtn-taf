/*
 * Created by Yehor Karliuchenko on 11/22/18 6:21 PM
 */

package ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import utils.Consts;
import utils.Log;

import java.net.MalformedURLException;
import java.net.URL;

import static java.util.concurrent.TimeUnit.SECONDS;

public enum Browser {
    EDGE,
    FIREFOX,
    GOOGLE_CHROME,
    INTERNET_EXPLORER,
    PROPERTY,
    REMOTE;

    public static final String GRID_HUB_URL = "http://10.27.100.30:4444/wd/hub";
    private static final String DEFAULT_BROWSER = "GC";

    static{
        System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY,
                           Consts.USER_DIR + "/src/main/resources/drivers/chromedriver.exe");
        System.setProperty(GeckoDriverService.GECKO_DRIVER_EXE_PROPERTY,
                           Consts.USER_DIR + "/src/main/resources/drivers/geckodriver.exe");
        System.setProperty(InternetExplorerDriverService.IE_DRIVER_EXE_PROPERTY,
                           Consts.USER_DIR + "/src/main/resources/drivers/IEDriverServer.exe");
        System.setProperty(EdgeDriverService.EDGE_DRIVER_EXE_PROPERTY,
                           Consts.USER_DIR + "/src/main/resources/drivers/MicrosoftWebDriver.exe");
    }

    public WebDriver getInstance(){
        WebDriver driver = null;
        if(this == GOOGLE_CHROME) driver = new ChromeDriver();
        if(this == FIREFOX) driver = new FirefoxDriver();
        if(this == INTERNET_EXPLORER) driver = new InternetExplorerDriver();
        if(this == EDGE) new EdgeDriver();
        if(this == REMOTE) {
            DesiredCapabilities capabilities = null;
            switch(System.getProperty("driver.type", DEFAULT_BROWSER)) {
                case "GC":
                    ChromeOptions chromeOptions = new ChromeOptions();
                    chromeOptions.addArguments("--window-size=1920,1080");
                    capabilities = DesiredCapabilities.chrome();
                    capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
                    break;
                case "FF":
                    capabilities = DesiredCapabilities.firefox();
                    break;
                case "IE":
                    capabilities = DesiredCapabilities.internetExplorer();
                    break;
                case "EG":
                    capabilities = DesiredCapabilities.edge();
                    break;
            }
            try {
                driver = new RemoteWebDriver(new URL(GRID_HUB_URL), capabilities);
            } catch(MalformedURLException e) {
                e.printStackTrace();
                Log.error("Malformed GRID URL");
            }

        }
        if(this == PROPERTY) {
            switch(System.getProperty("driver.type", DEFAULT_BROWSER)) {
                case "GC":
                    driver = new ChromeDriver();
                    break;
                case "FF":
                    driver = new FirefoxDriver();
                    break;
                case "IE":
                    driver = new InternetExplorerDriver();
                    break;
                case "EG":
                    driver = new EdgeDriver();
                    break;
            }
        }
        driver.manage().timeouts().setScriptTimeout(60, SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, SECONDS);

        //TODO window().maximize causes remote browser to shrink making the app work in a tablet mode.
        //driver.manage().window().maximize();
        return driver;
    }
}
