/*
 * Created by Yehor Karliuchenko on 11/23/18 2:19 PM
 */

package ui;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ui.rtnhub.pages.LoginPage;
import ui.rtnhub.pages.HomePage;

import static utils.Vars.*;

public class PagesLoadCheckUp {

    private WebDriver driver;
    private HomePage homePage;

    @BeforeClass
    private void setUp(){
        driver = Browser.REMOTE.getInstance();
        driver.get(RTNHUB_BASE_URL);
        homePage = new LoginPage(driver).signIn(WSU_USERNAME, WSU_PASSWORD);
    }

    @AfterClass
    private void tearDown() {
        driver.quit();
    }

    @Test
    private void test01_checkUsersPageLoads() {
        homePage.navSideBar.openUsers();
    }

    @Test
    private void test02_checkKeyTagsPageLoads() {
        homePage.navSideBar.openKeyTags();
    }

    @Test
    private void test03_checkKeyTagGroupsPageLoads() {
        homePage.navSideBar.openKeyTagGroups();
    }

    @Test
    private void test04_checkItemsPageLoads() {
        homePage.navSideBar.openItems();
    }

    @Test
    private void test05_checkItemGroupsPageLoads() {
        homePage.navSideBar.openItemGroups();
    }

    @Test
    private void test06_checkSmartTerminalPageLoads() {
        homePage.navSideBar.openSmartTerminal();
    }

    @Test
    private void test07_checkCalendarViewPageLoads() {
        homePage.navSideBar.openCalendarReservations();
    }

    @Test
    private void test08_checkReportsPageLoads() {
        homePage.navSideBar.openReports();
    }

    @Test
    private void test09_checkLdapPageLoads() {
        homePage.navSideBar.openLdap();
    }

    @Test
    private void test10_checkEmailServerPageLoads() {
        homePage.navSideBar.openEmailServer();
    }

    @Test
    private void test11_checkS2NetboxPageLoads() {
        homePage.navSideBar.openS2Netbox();
    }

    @Test
    private void test12_checkAdminDataPageLoads() {
        homePage.navSideBar.openAdminData();
    }

    @Test
    private void test13_checkAdminSettingsPageLoads() {
        homePage.navSideBar.openAdminSettings();
    }

    @Test
    private void test14_checkAttributeListPageLoads() {
        homePage.navSideBar.openAttributeList();
    }

    @Test
    private void test15_checkAttributeDefinitionPageLoads() {
        homePage.navSideBar.openAttributeDefinition();
    }
}
